package animals;

// FROG

public class AmolopsKaulbacki extends Animal implements Amphibian {

	public AmolopsKaulbacki(String name) throws EmptyNameException {
		super(name);
	}

	@Override
	public void swim() {
		System.out.println(name + " the frog is swimming.");
	}

	@Override
	public void walk() {
		System.out.println(name + " the frog is walking.");
	}

}
