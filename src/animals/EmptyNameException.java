package animals;

public class EmptyNameException extends Exception {
	public EmptyNameException(String className) {
		super(className + " cannot have an empty name!");
	}
}
