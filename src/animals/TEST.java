package animals;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class TEST {

	public static void main(String[] args) throws EmptyNameException {
		
		// Declare number of animals per species
		int numberOfAnimalsPerSpecies = 3;
		
		Map<Integer, String> names = new HashMap<>();
		names.put(0, "Mirek");
		names.put(1, "Bob");
		names.put(2, "Marysia");
		names.put(3, "Andrzej");
		names.put(4, "Brajan");
		names.put(5, "Gerard");
		names.put(6, "Ola");
		names.put(7, "Jadwiga");
		names.put(8, "Kajtek");
		
		// Delare containters
		LinkedList<Swimmable> swimmables = new LinkedList<>();
		LinkedList<Walkable> walkables = new LinkedList<>();
		LinkedList<Flyable> flyables = new LinkedList<>();
		
		// Add named animals to specified containers
		for (int i = 0; i < numberOfAnimalsPerSpecies * 3; i += 3) {
			AmolopsKaulbacki 		frog = new AmolopsKaulbacki(names.get(i));
			CheilopogonCyanopterus 	flyingFish = new CheilopogonCyanopterus(names.get(i + 1));
			CiconiaCiconia 			stork = new CiconiaCiconia(names.get(i + 2));
			
			// Add animals to lists
			swimmables.add(frog);
			swimmables.add(flyingFish);
			
			walkables.add(frog);
			walkables.add(stork);
			
			flyables.add(flyingFish);
			flyables.add(stork);
		}
		
		// Swim, walk and fly
		for (Swimmable swimmable : swimmables)
			swimmable.swim();
		for (Walkable walkable : walkables)
			walkable.walk();
		for (Flyable flyable : flyables)
			flyable.fly();
		
	}

}
