package animals;

public abstract class Animal extends justARandomClassToFulfill2LevelClassInheritanceRequirement {
	
	protected String name;
	
	public Animal(String name) throws EmptyNameException {
		if (name == "")
			throw new EmptyNameException(this.getClass().getSimpleName());
		else
			this.name = name;
	}
}