package animals;

// FLYING FISH

public class CheilopogonCyanopterus extends Animal implements FlyingFish {

	public CheilopogonCyanopterus(String name) throws EmptyNameException {
		super(name);
	}

	@Override
	public void swim() {
		System.out.println(name + " the flying fish is swimming.");
	}

	@Override
	public void fly() {
		System.out.println(name + " the flying fish is flying.");
	}

}
