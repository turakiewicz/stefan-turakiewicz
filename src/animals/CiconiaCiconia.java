package animals;

// STORK

public class CiconiaCiconia extends Animal implements Bird {

	public CiconiaCiconia(String name) throws EmptyNameException {
		super(name);
	}

	@Override
	public void walk() {
		System.out.println(name + " the stork is walking.");
	}

	@Override
	public void fly() {
		System.out.println(name + " the stork is flying.");
	}

}
