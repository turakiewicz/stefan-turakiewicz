##Author

Stefan Turakiewicz



##What the program does

The program consists of some interfaces (Walkable, Flyable, Swimmable),
that are extended by other interfaces (Bird, FlyingFish, Amphibian), that are
implemented by species classes (AmolopsKaulbacki and so on). Species do also
extend an abstract class Animal, which contains a name (String) and constructor
assigning that name.

The main function is implemented in TEST class. It performs a short presentation of
functionalities listed above. First there are created some animals, then the animals
methods are called to show how every species react differently at calling functions
with the same name.



##Additional info:

To make use of the custom exception, you should change one of the names put into the
names map in TEST.java to an empty string.